# Server Relay

This application facilitates local development using frontend frameworks such as Vue, React, Svelte or Angular with a remote API protected by an HTTP password. It thus lets many developers to share the same password-protected API.

It handles JSON and formData for POST transfers and JSON only for PUT requests.

The application also supports file uploads via the Multer module.

You can configure the backend API, http user name and password and local port in config/config.js .