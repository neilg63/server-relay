const request = require("request-promise-native");
const fs = require("fs");
const path = require("path");
const Base64 = require('base-64');
const config = require('../config/config');

const fetchData = (url, req, res) => {
  // url, method, data, json, formData, req
  getSourceData(url + addQueryString(req), 'GET', {}, {}, {}, req).then(response => {
    res.send(response);
  })
}

const postData = (url, req, res, data, json) => {
  let formData = {};
  if (url.indexOf('/upload') >= 0 || url.indexOf('submission/file/') >= 0) {
    if (req.file) {
      json = false;
      const { file } = req;
      if (file) {
        /* let fp = "./uploads/" + file.name;
        fs.writeFileSync(fp, file.data.toString());
        file.mv(fp); */
        formData = {
          file: {
            value: fs.createReadStream(file.path),
            options: {
              filename: file.originalname,
              contentType: file.mimetype
            }
          }
        }
      }
    }
  }

  getSourceData(url + addQueryString(req), 'POST', data, json, formData, req).then(response => {
    res.send(response);
  })
}

const putData = (url, req, res, data) => {
  getSourceData(url + addQueryString(req), 'PUT', data, {}, {}, req).then(response => {
    res.send(response);
  })
}

const deleteData = (url, req, res, data) => {
  getSourceData(url + addQueryString(req), 'DELETE', data, {}, {}, req).then(response => {
    res.send(response);
  })
}

const addQueryString = (req) => {
  let qs = '';
  if (req.query) {
    const parts = Object.entries(req.query).map(entry => `${entry[0]}=${entry[1]}`)
    if (parts.length > 0) {
      qs = '?' + parts.join('&');
    }
  }
  return qs;
}

const buildHeaders = (req, json) => {
  let hds = {};
  if (req instanceof Object) {
    Object.entries(req.headers).forEach(entry => {
      const [key, val] = entry;
      switch (key) {
        case 'token':
        case 'Content-Type':
          hds[key] = val;
          break;
      }
    });
  }
  if (config.httpAuth) {
    const { user, pass } = config.httpAuth;
    if (user && pass) {
      const hash = Base64.encode(user + ':' + pass);
      const Basic = 'Basic ' + hash;
      hds.Authorization = Basic
    }
    hds.apiKey = "dkdywe_w278";
    hds.clientId = "1827373mf";
  }
  return hds;
}

const getSourceData = async (url, method, data, json, formData, req) => {
  if (!method) {
    method = 'GET';
  }
  if (json !== false) {
    json = true;
  }
  let options = {
    method: method,
    uri: url,
    json: json,
    headers: buildHeaders(req, json)
  }
  console.log(url);
  if (data instanceof Object) {
    if (Object.keys(data).length > 0) {
      options.body = data;
    }
  }
  if (formData instanceof Object) {
    if (formData.file) {
      options.formData = formData;
    }
  }

  return await request(options)
    .then(response => {
      return response;
    })
    .catch(error => {
      return {
        error: error
      };
    });
}

module.exports = { fetchData, postData, putData, deleteData, getSourceData };