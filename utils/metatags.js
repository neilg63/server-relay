const redis = require("redis");
const client = redis.createClient();
const { promisify } = require("util");
const getAsync = promisify(client.get).bind(client);
const fs = require("fs");
const config = require("../config/config");

const attrsToString = attrs => {
  let str = "";
  if (typeof attrs === "object" && attrs !== null) {
    let keys = Object.keys(attrs);

    let strs = keys
      .map(k => {
        let str = "";
        if (typeof attrs[k] === "string") {
          str = k + '="' + attrs[k] + '"';
        }
        return str;
      })
      .filter(s => s.length > 3);
    if (strs.length > 0) {
      str += " " + strs.join(" ");
    }
  }
  return str;
};

const htmlTag = (tagName, attrs, content) => {
  let str = "<" + tagName + attrsToString(attrs);
  if (typeof content === "string") {
    str += ">" + content + "</" + tagName + ">";
  } else {
    str += " />";
  }
  return str;
};

const titleTag = content => {
  return htmlTag("title", {}, content);
};

const metaTag = attrs => htmlTag("meta", attrs);

const metaProp = (name, content) => {
  let attrs = {
    name,
    content
  };
  return metaTag(attrs);
};

const linkTag = attrs => htmlTag("link", attrs);

const linkRelTag = (relType, href) => {
  let attrs = {
    rel: relType,
    href: href
  };
  return linkTag(attrs);
};

const twitterTag = (relType, content) => {
  let attrs = {
    name: "twitter:" + relType,
    content: content
  };
  return metaTag(attrs);
};

const ogTag = (relType, content) => {
  let attrs = {
    property: "og:" + relType,
    content: content
  };
  return metaTag(attrs);
};

const fetchPageMeta = async (slug, refresh) => {
  let mSlug = slug.length > 3 ? slug : 'index';
  switch (slug) {
    case "index":
      mSlug = "project";
      break;
  }
  let cKey = mSlug + "-meta";
  let html = "";
  if (refresh !== true) {
    await getAsync(cKey).then(res => {
      if (typeof res === "string") {
        if (res.length >= 20 && res.includes("<meta")) {
          html = res;
        }
      }
    });
  }
  if (html.length < 20) {
    html = await renderPageMeta(mSlug);
    client.set(cKey, html, "EX", 86400);
  }
  return html;
};

const renderPageMeta = async (alias) => {
  let html = "";
  const { getSourceData } = require("./fetch");
  const data = await getSourceData(config.cms.siteUrl);

  if (data.pages instanceof Array) {
    if (/\bproducts\/\w+/.test(alias)) {
      page = data.products.find(item => {
        let matched = item.slug === alias;
        if (!matched && alias.indexOf('/') > 2 && alias.length > 5) {
          matched = item.slug === alias.split('/').pop();
        }
        return matched;
      });
    } else {
      page = data.pages.find(item => item.slug === alias);
    }
    if (!page) {
      page = data.pages.find(item => item.slug === 'about');
    }
    if (page) {
      let url = config.baseFrontUrl + "/" + alias;
      let params = {
        title: page.title,
        description: page.excerpt,
        imgUri: page.image_src,
        url: url
      };
      let tgs = fetchCoreMetaTags(page.title);
      tgs = tgs.concat(pageMetaTags(params));
      html = tgs.join("");
    }
  }
  return html;
};

const fetchCoreMetaTags = title => {
  let tgs = [];
  tgs.push(titleTag(config.siteName + ": " + title));
  tgs.push(metaProp("description", config.description));
  tgs.push(metaProp("keywords", config.keywords));

  tgs.push(ogTag("type", "website"));
  tgs.push(ogTag("site_name", config.siteName));

  tgs.push(twitterTag("card", "summary"));
  return tgs;
};

const pageMetaTags = params => {
  let tgs = [];
  let { title, description, url, imgUri } = params;
  tgs.push(ogTag("title", title));
  tgs.push(ogTag("image", imgUri));
  tgs.push(ogTag("url", url));
  tgs.push(twitterTag("description", description));
  tgs.push(twitterTag("title", title));
  tgs.push(twitterTag("image", imgUri));
  tgs.push(twitterTag("url", url));
  return tgs;
};


const matchPath = (alias, cType) => {
  let path = config.includeRoot + "/" + alias + "__" + cType + ".html";
  if (!fs.existsSync(path)) {
    path = "";
  }
  return path;
}

const fetchPath = async (slug, cType) => {
  let alias = slug.replace(/^\//, "").replace(/\/+/g, "__");
  let path = matchPath(alias, cType)
  if (path.length < 2) {
    let parts = alias.split('__');
    if (parts.length > 1) {
      alias = parts.shift();
      path = matchPath(alias, cType);
    }
    if (path.length < 2) {
      path = matchPath('index', cType);
    }
  }
  return path;
};

module.exports = {
  fetchPageMeta,
  fetchPath
};
