const express = require('express');
const config = require("../config/config");
const { fetchData, postData, putData, deleteData } = require("../utils/fetch");
const router = express.Router();

const multer = require("multer");
const upload = multer({ dest: "uploads" });


router.get("/:one?/:two?/:three?/:four?/:five?", (req, res) => {
  let url = config.api;
  let { one, two, three, four, five } = req.params;
  if (one) {
    url += '/' + one;
    if (two) {
      url += '/' + two;
      if (three) {
        url += '/' + three;
        if (four) {
          url += '/' + four;
          if (five) {
            url += '/' + five;
          }
        }
      }
    }
  }
  fetchData(url, req, res);
});

router.post("/:one?/:two?/:three?/:four?", upload.single('file'), (req, res) => {
  let url = config.api;
  let { one, two, three, four } = req.params;
  if (one) {
    url += '/' + one;
    if (two) {
      url += '/' + two;
      if (three) {
        url += '/' + three;
        if (four) {
          url += '/' + four;
        }
      }
    }
  }
  postData(url, req, res, req.body);
});

router.put("/:one?/:two?/:three?/:four?/:five?", (req, res) => {
  let url = config.api;
  let { one, two, three, four, five } = req.params;
  if (one) {
    url += '/' + one;
    if (two) {
      url += '/' + two;
      if (three) {
        url += '/' + three;
        if (four) {
          url += '/' + four;
          if (five) {
            url += '/' + five;
          }
        }
      }
    }
  }
  putData(url, req, res, req.body);
});

router.delete("/:one?/:two?/:three?/:four?/:five?", (req, res) => {
  let url = config.api;
  let { one, two, three, four, five } = req.params;
  if (one) {
    url += '/' + one;
    if (two) {
      url += '/' + two;
      if (three) {
        url += '/' + three;
        if (four) {
          url += '/' + four;
          if (five) {
            url += '/' + five;
          }
        }
      }
    }
  }
  deleteData(url, req, res, req.body);
});


module.exports = router;